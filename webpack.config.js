const path = require("path");
const webpack = require("webpack");

// TODO: Should be moved to production config.
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const extractPlugin = new ExtractTextPlugin({
//     filename: 'bundle.css'
// })

module.exports = {
  mode: 'development',
  entry: "./src/index.ts",
  devtool: "inline-source-map",
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: [
          path.resolve(__dirname, 'src/css')
        ],
        use: [
          {
            loader: "css-loader",
            options: { importLoaders: 1 }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [ require("postcss-preset-env") ]
            }
          }
        ]
      },
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  plugins: [],
  output: {
    path: path.resolve(__dirname, "dist/"),
    filename: "bundle.js",
    publicPath: "/dist"
  }
};
