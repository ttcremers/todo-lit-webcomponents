export interface Todo {
  readonly index: number;
  readonly text: string;
  readonly completed: boolean;
}