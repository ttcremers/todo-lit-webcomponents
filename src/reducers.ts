import {
  VisibilityFilters,
  SET_VISIBILITY_FILTER,
  ADD_TODO,
  TOGGLE_TODO,
  ActionMessage
} from "./actions";

import { Todo } from "./model";
import { combineReducers } from "redux";
import { mockData } from "./mock-data";

export interface StateShape {
  visibilityFilter: string;
  todos: Array<Todo>;
}

// Default initial state of our TODO app.
const initialState = {
  visibilityFilter: VisibilityFilters.SHOW_ALL,
  todos: mockData,
} as StateShape;

/*
 * Reducer composition for `todos`.
 */
const todos = (state: Array<Todo> = initialState.todos, action: ActionMessage) => {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          text: action.text,
          completed: false,
        }
      ];

    case TOGGLE_TODO:
      return state.map((todo, index: number) => {
        if (index === action.index) {
          return { ...todo, completed: !todo.completed };
        }
        return todo;
      });

    default:
      return state;
  }
};

/*
 * Reducer composition for `visibilityFilter`.
 */
const { SHOW_ALL } = VisibilityFilters;
const visibilityFilter = (state = SHOW_ALL, action: ActionMessage ) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;

    default:
      return state;
  }
};

/*
 * Main app reducer for global state.
 */
const todoApp = combineReducers({ visibilityFilter, todos });
export default todoApp;
