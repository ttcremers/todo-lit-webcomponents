// (REDUX) Action type definnitions.
export const ADD_TODO = "ADD_TODO";
export const REMOVE_TODO = "REMOVE_TODO";
export const EDIT_TODO = "EDIT_TODO";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const SET_VISIBILITY_FILTER = "TOGGLE_TODO";

// Definnition of filter values. (I love TypeScript).
export const VisibilityFilters: {
  readonly SHOW_ALL: string;
  readonly SHOW_COMPLETED: string;
} = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED"
};

// Action message interface/definition.
export interface ActionMessage {
  readonly type:string;
  readonly text:string;
  readonly index:number;
  readonly filter:boolean;
}

// The actions.
export const addTodo = (text:string):ActionMessage => {
  return { type: ADD_TODO, text } as ActionMessage;
};

export const toggleTodo = (index:number):ActionMessage => {
  return { type: EDIT_TODO, index } as ActionMessage;
};

export const setVisibilityFilter = (filter:boolean):ActionMessage => {
  return { type: SET_VISIBILITY_FILTER, filter } as ActionMessage;
}
