import { Todo } from "./model";

export const mockData:Array<Todo> = [
  {
    index: 0,
    completed: false,
    text: "Learn more TypeScript sir!"
  } as Todo,
  {
    index: 1,
    completed: false,
    text: "Fix more bugs."
  } as Todo,
  {
    index: 2,
    completed: true,
    text: "Write more sensible todo's"
  } as Todo,
  {
    index: 3,
    completed: false,
    text: "Write a couple of longer todo's so they are not all the same length."
  } as Todo,
];