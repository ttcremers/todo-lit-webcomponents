import { LitElement, html, property } from '@polymer/lit-element';

// Just to show that polymer components work.
import '@material/mwc-button';

import sharedStyles from '../my-shared-styles/my-shared-styles';

// We can use postcss in these files. Still need something better than this.
import style from './my-todo-input.css';

class MyTodoInput extends LitElement {
  @property({ type: String })
  todo: string = '';

  @property({ type: String })
  _error: string = '';

  render() {
    const { todo, _error, _addButtonClicked } = this;
    return html`
      ${sharedStyles}
      <style>
        ${style.toString()}
      </style>
      <div class="todo-add-input">
        <input type="text" .value="${todo}">
        <mwc-button
          @click="${_addButtonClicked}"
          outlined>
          Add Todo
        </mwc-button>
      </div>
      <span class="error">${_error}</span>
    `;
  }

  _addButtonClicked(event: Event) {
    const todo = this.shadowRoot.querySelector('input').value;
    if (todo.length) {
      this._error = '';
      this.dispatchEvent(new CustomEvent('todo-added', { detail: { todo } }));
      this.shadowRoot.querySelector('input').value = '';
    } else {
      this.__showError('Please fill in a todo.');
    }
  }

  __showError(error: string): void {
    this._error = error;
  }
}
window.customElements.define('my-todo-input', MyTodoInput);
