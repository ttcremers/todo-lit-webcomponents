import { LitElement, html, property } from "@polymer/lit-element/";
import { connect } from "pwa-helpers/connect-mixin";
import { addTodo, toggleTodo } from "../../actions";
import { store } from "../../store";
import { StateShape } from "../../reducers";
import { Todo } from "../../model";

import '../my-element/my-element'
import '../my-todo-input/my-todo-input';
import '../my-todo/my-todo'

import sharedStyles from "../my-shared-styles/my-shared-styles";

class MyApp extends connect(store)(LitElement) {
  @property({ type:Array })
  todos:Array<Todo> = [];

  render() {
    return html`
      ${sharedStyles}
      <style>
        :host {
          font-family: 'Roboto', sans-serif;

          /* Our local app definitions. */
          --app-primary-color: #e91e63;
          --app-secondary-color: #1ee9a4;
          --app-dark-text-color: var(--app-secondary-color);
          --app-light-text-color: white;
          --app-section-even-color: #f7f7f7;
          --app-section-odd-color: white;
          --app-warning-color: orange;
          --app-error-color: red;

          /* Overwrite on 3de party components. */
          --mdc-theme-primary: var(--app-primary-color);
          --mdc-theme-secondary: var(--app-secondary-color);
        }
      </style>
      <div class="main">
        <my-todo-input @todo-added="${this._addTodo}"></my-todo-input>
        <div class="todo-list">
          ${this.todos.map((todo:Todo) => html`
            <my-todo
              @status-changed="${this._todoStatusChanged}"
              .index="${todo.index}"
              .text="${todo.text}"
              .completed="${todo.completed}">
            </my-todo>
          `)}
        </div>
      </div>
      <footer>
        <my-element></my-element>
      </footer>
    `;
  }

  stateChanged(state:StateShape) {
    this.todos = state.todos;
  }

  _addTodo(event:CustomEvent):void {
    store.dispatch(addTodo(event.detail.todo));
  }

  _todoStatusChanged(event:CustomEvent):void {
    store.dispatch(toggleTodo(event.detail.index));
  }
}
window.customElements.define("my-app", MyApp);
