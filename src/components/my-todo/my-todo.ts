import {
  LitElement,
  html,
  property
} from "@polymer/lit-element/";

import '@material/mwc-switch';

import postcss from './my-todo.css';

class MyElement extends LitElement {
  @property({ type: String })
  text: string = '';

  @property({ type: Boolean })
  completed: boolean = false;

  @property({ type: Number })
  index: number = -1;

  render() {
    return html`
      <style>${postcss.toString()}</style>
      <div class="todo">
        <span class="text">${this.text}</span>
        <mwc-switch .checked="${this.completed}" @click="${this._doneClicked}">
        </mwc-switch>
      </div>
    `;
  }

  _doneClicked() {
    this.dispatchEvent(
      new CustomEvent("status-changed", {
        detail: { index: this.index, completed: this.completed } }
      )
    );
  }
}
window.customElements.define('my-todo', MyElement);