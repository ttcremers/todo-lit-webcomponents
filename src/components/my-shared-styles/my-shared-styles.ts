import { html } from "@polymer/lit-element";

export default html`
  <style>
    /* Styles shared across components. */
    :host {
      display: block;
    }
  </style>
`;
