import {
  LitElement,
  html,
  property
} from "@polymer/lit-element/";

import postcss from './my-element.css';

class MyElement extends LitElement {

  @property({ type: String })
  mood: string = "HAPPY!";

  render() {
    return html`
      <style>${postcss.toString()}</style>

      Web Components are <span class="mood">${this.mood}</span>!
    `;
  }
}
window.customElements.define('my-element', MyElement);