Use the platform ❤️
==================

Introduction
------------
Example todo app for demonstrating the use of the WebComponent spec (https://www.w3.org/wiki/WebComponents/) 

Aside from using **TypeScript** (https://www.typescriptlang.org/) and **PostCSS** (https://postcss.org/) all Javascript APIs are native to the browser (Use the platform ❤️). 
Some sugar around those APIs is used in the form of **Polymer Lit Element** (https://lit-element.polymer-project.org/) 

And what would a basic todo app do without Redux... 🤨 (https://redux.js.org/)

_This project serves as an example of what a project like this might look like._

Feel free to do with it as you please!

Usage
-----

In the root of the project:

```
$ yarn install
```

And to compile and start the dev server:

```
$ yarn serve
```

Point your browser to: http://locahost:8080/

**That's it!**

Notes
-----

**Scoped styles**

If you're unfamiliar with the Webcomponent spec you should be aware that component styles are scoped. Don't expect to just drop in Bootstrap or Tailwind and expect everything to work. 

'_Shadow DOM fixes CSS and DOM. It introduces scoped styles to the web platform. Without tools or naming conventions, you can bundle CSS with markup, hide implementation details, and author self-contained components in vanilla JavaScript._'

for more info https://developers.google.com/web/fundamentals/web-components/shadowdom

**You do not need all this**

You could make this app in a single html file with vanilla javascript and get rid of Webpack, TypeScript and Polymer Lit Element and still be able to implement these components and run it in a (reasonable modern) browser.

You can find a more friendly introduction to Web Components at css-tricks.com here: https://css-tricks.com/an-introduction-to-web-components/

**You are using it already**

The Youtubes uses Web Components! https://youtube.com

License
-------

As I said, do with it as you please:

MIT License

Copyright (c) 2020 Thomas T. Cremers

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

